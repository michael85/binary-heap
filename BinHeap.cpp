//
//  BinHeap.cpp
//

#include "BinHeap.h"
#include "Flags.h"

/* **************************************************************** */

#if CONSTRUCTOR || ALL
template <class T>
BinHeap<T>::BinHeap()
{
  heapArray = new T[2];
  heapArray[0] = -1;
  maxSize = 1;
  heapSize = 0;
}
#endif

/* **************************************************************** */

#if DESTRUCTOR || ALL
template <class T>
BinHeap<T>::~BinHeap()
{
  delete[] heapArray;
}
#endif

/* **************************************************************** */

#if ISEMPTY || ALL
template <class T>
bool BinHeap<T>::isEmpty(){
  return heapSize == 0;
}
#endif

/* **************************************************************** */

#if MAKEEMPTY || ALL
template <class T>
void BinHeap<T>::makeEmpty(){
  delete[] heapArray;
  heapArray = new T[2];
  heapArray[0] = -1;
  maxSize = 1;
  heapSize = 0;
}
#endif

/* **************************************************************** */

#if RESIZEARRAY || ALL
template <class T>
void BinHeap<T>::resizeArray(int newSize){
  T* newArray = new T[newSize + 1];
  for(int i = 0; i < heapSize + 1; i++){
    newArray[i] = heapArray[i];
  }
  delete[] heapArray;
  heapArray = newArray;
  maxSize = newSize;
}
#endif

/* **************************************************************** */

#if LEFTINDEX || ALL
template <class T>
int BinHeap<T>::leftIndex(int idx){
  return 2 *idx;
}
#endif

/* **************************************************************** */

#if RIGHTINDEX || ALL
template <class T>
int BinHeap<T>::rightIndex(int idx){
  return (2 * idx) + 1;
}
#endif

/* **************************************************************** */

#if PARENTINDEX || ALL
template <class T>
int BinHeap<T>::parentIndex(int idx){
  return idx/2;
}
#endif

/* **************************************************************** */

#if MINCHILD || ALL
template <class T>
int BinHeap<T>::minChild(int idx){
  //no children
  if(leftIndex(idx) > heapSize){
    return -1;
  }
  //left children only
  else if(rightIndex(idx) > heapSize){
    return leftIndex(idx);
  }
  else{
    return (heapArray[leftIndex(idx)] < heapArray[rightIndex(idx)]) ? leftIndex(idx) : rightIndex(idx);
  }
}
#endif

/* **************************************************************** */

#if INSERT || ALL
template <class T>
void BinHeap<T>::insert(const T & x){
  if(heapSize == maxSize){
    resizeArray(heapSize * 2);
  }
  heapSize++;
  heapArray[heapSize] = x;
  percolateUp(heapSize);
}
#endif

/* **************************************************************** */

#if REMOVEMIN || ALL
template <class T>
T BinHeap<T>::removeMin(){
  if(heapSize == 0){
    return -1;
  }else{
    T temp = heapArray[1];
    heapArray[1] = heapArray[heapSize];
    heapArray[heapSize] = temp;
    heapSize--;
    percolateDown(1);
    //heapSize--;
    if( (maxSize / 3) > heapSize ){
      resizeArray(maxSize / 2);
    }
    return temp;
  }
}
#endif

/* **************************************************************** */

#if PERCOLATEUP || ALL
template <class T>
void BinHeap<T>::percolateUp(int idx){
  while(idx != 0){
    int parent = parentIndex(idx);
    if( heapArray[idx] < heapArray[parent] ){
      T temp = heapArray[parent];
      heapArray[parent] = heapArray[idx];
      heapArray[idx] = temp;
    }
    idx--;
  }
}
#endif

/* **************************************************************** */

#if PERCOLATEDOWN || ALL
template <class T>
void BinHeap<T>::percolateDown(int idx){
  while(minChild(idx) != -1){
    int min = minChild(idx);
    if(min != -1){
      if( heapArray[idx] > heapArray[min] ){
	T temp = heapArray[min];
	heapArray[min] = heapArray[idx];
	heapArray[idx] = temp;
      }
    }
    idx++;
  }
}
#endif

/* **************************************************************** */

#if BUILDHEAP || ALL
template <class T>
void BinHeap<T>::buildHeap(const T* arr, int size){
  int i = 1;
  resizeArray(size);
  heapSize = size;
  maxSize = size;
  while(i != size + 1){
    heapArray[i] = arr[i-1];
    i++;
  }
  i = size / 2;
  while(i != 0){
    percolateDown(i);
    i--;
  }
}
#endif

/* **************************************************************** */



/* **************************************************************** */
/* Do NOT modify anything below this line                           */
/* **************************************************************** */

#ifndef BUILD_LIB
template <class T>
void BinHeap<T>::printHeap()
{
    std::cout << "Current array size: " << maxSize << std::endl;
    std::cout << "Current heap size: " << heapSize << std::endl;
    if (!isEmpty())
    {
        std::cout << "Heap Elements: ";
        // Print heap array
        for (int i = 1; i <= heapSize; i++) {
            std::cout << heapArray[i] << " ";
        }
    }
    else
    {
        std::cout << "Heap is empty";
    }
    std::cout << std::endl << std::endl;
}
#endif

template class BinHeap<int>;
