# Binary Heap

A binary heap that stores arbitrary objects via class templates. 
The heap will use a templated array backing structure whose size will be 
dynamically adjusted depending on the size of the heap being stored.

http://ycpcs.github.io/cs350-fall2014/assign/assign07.html